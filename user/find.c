#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"
#include "kernel/fs.h"

void find(char *path, char *fName);

// 在目录树中查找名称与字符串匹配的所有文件，输出文件的相对路径。
// 命令格式：find path file_name
int main(int argc,char* argv[]){
    if (argc != 3){
        printf("usage:find dirName fileName");
        exit(1);
    }
    find(argv[1],argv[2]);
    exit(0);
}

// 关键的查找函数
void find(char *path, char *fName){
    char buffer[512], *p;
    int fd;
    struct dirent de;
    struct stat st;

    if((fd = open(path, 0)) < 0){        // 首先打开文件
        fprintf(2, "find: cannot open %s\n", path);
        return;
    }
    if(fstat(fd, &st) < 0){     // 获取文件描述符的信息
        printf("find: cannot stat %s\n", path);
        close(fd);
        return;
    }

    if (st.type != T_DIR){              // 检测输入的dir是否为目录
        printf("find:%s is not a directory\n", path);
        close(fd);
        return;
    }

    if(strlen(path) + 1 + DIRSIZ + 1 > sizeof buffer){     // 判断路径长度是否符合要求
        printf("find: directory too long\n");
        close(fd);
        return;
    }

    strcpy(buffer,path);        // 复制path到buffer
    p = buffer + strlen(buffer);  // 将p指针指向buffer的最后一位
    *p = '/';               // 将最后一位字符赋值为‘/’
    p++;                    // 指针指向下一位，用来接收之后的信息
    // 从文件描述符（fd）对应的文件中读取数据，写到de中
    while(read(fd, &de, sizeof(de)) == sizeof(de)){         // 读取目录下的信息
        if(de.inum == 0)
            continue;

        // strcmp()，当两个字符串相等时，返回0
        if(!strcmp(de.name,".") || !strcmp(de.name,"..")){  // 不要递归'.','..'
            continue;
        }

        // memmove()，将de.name移动到p的位置
        memmove(p, de.name, DIRSIZ);
        p[DIRSIZ] = 0;  // 在缓冲区buffer的末尾添加一个空字符，以确保字符串以空字符结尾。
        if(stat(buffer, &st) < 0){
            printf("find: cannot stat %s\n", buffer);
            continue;
        }
        switch(st.type){                    // 判断读入的信息为目录还是文件
            case T_FILE:
                if(!strcmp(de.name,fName)){
                    printf("%s\n",buffer);
                }
                break;
            case T_DIR:
                find(buffer,fName);
                break;
        }
    }
    close(fd);
}