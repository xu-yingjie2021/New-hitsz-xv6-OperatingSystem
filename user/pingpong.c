#include "kernel/types.h"
#include "user.h"   // 导入xv6自带的库函数

int main(int argc,char* argv[]){
    int p1[2], p2[2];
    pipe(p1);
    pipe(p2);
    char buffer[4];
    int size = 4;
    // 管道创建完成之后，0为读，1为写

    // // 方法一：使用两个pipe管道
    // // 创建子进程
    // if(fork() == 0){
    //     // 子进程，fork()返回0
    //     close(p1[1]);   // 关闭子进程p1写端
    //     read(p1[0], buffer, size);  // 读取
    //     printf("%d: received %s\n", getpid(),buffer);
    //     close(p1[0]);

    //     close(p2[0]);   // 关闭子进程p2读端
    //     write(p2[1], "pong", strlen("pong"));   // 写入
    //     close(p2[1]);
    //     exit(0);
    // }else{
    //     // 父进程，fork()返回子进程的pid
    //     close(p1[0]);   // 关闭父进程p1读端
    //     write(p1[1], "ping", strlen("ping"));   // 写入
    //     close(p1[1]);

    //     close(p2[1]);
    //     read(p2[0], buffer, size);
    //     printf("%d: received %s\n", getpid(),buffer);
    //     close(p2[0]);
    //     exit(0);
    // }

    // 方法二：使用一个pipe管道
    // 创建子进程
    if(fork() == 0){
        read(p1[0],buffer,size);
        printf("%d: received %s\n", getpid(),buffer);
        write(p1[1],"pong",strlen("pong"));
        exit(0);
    }
    else{
        write(p1[1],"ping",strlen("ping"));
        wait(0);     // 等待子进程完成读写操作
        read(p1[0],buffer,size);
        printf("%d: received %s\n",getpid(),buffer);
        exit(0);
    }
}